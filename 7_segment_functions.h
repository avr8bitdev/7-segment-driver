/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SEGMENT_7_HELPER_FUNCTIONS_H_
#define SEGMENT_7_HELPER_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../../MCAL_drivers/DIO_driver/DIO_functions.h"

#define SEG7_SEGMENTS_COUNT_ 7

typedef struct
{
	DIO_port_t port7Seg; // 7-segment port

	u8 pin7Seg[SEG7_SEGMENTS_COUNT_]; // pins numbers
	/*
	          p[0]
	         ------
	        |      |
	   p[6] | p[5] | p[1]
	        |------|
	   p[4] |      | p[2]
	        | p[3] |
	         ------
	 */


	u8 seg7_off_state; // 1: common anode, 0: common cathode
} Seg7_obj_t;

typedef const Seg7_obj_t* Seg7_cptr_t;

void seg7_vidInit(Seg7_cptr_t structPtr7segCpy);

void seg7_vidPrintDigit(const u8 u8NumCpy, Seg7_cptr_t structPtr7segCpy);

void seg7_vidTurnOff(Seg7_cptr_t structPtr7segCpy);

// TODO: implement this
//void seg7_vidPrintPattern(const u8 u8ArrayPatternCpy[7], Seg7_cptr_t structPtr7segCpy);


#endif /* SEGMENT_7_HELPER_FUNCTIONS_H_ */

