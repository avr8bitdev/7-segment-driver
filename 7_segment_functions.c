/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "7_segment_functions.h"


void seg7_vidInit(Seg7_cptr_t structPtr7segCpy)
{
	for (u8 i = 0; i < SEG7_SEGMENTS_COUNT_; i++)
	{
		DIO_vidSet_pinDirection(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[i], OUTPUT);
		DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[i], structPtr7segCpy->seg7_off_state);
	}
}

void seg7_vidPrintDigit(const u8 u8NumCpy, Seg7_cptr_t structPtr7segCpy)
{
	// turn off all LEDs
	seg7_vidTurnOff(structPtr7segCpy);

	// turn on used LEDs
	switch (u8NumCpy)
	{
		case 0:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[0], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[1], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[3], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[4], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[6], !(structPtr7segCpy->seg7_off_state));
		break;

		case 1:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[1], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
		break;

		case 2:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[0], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[1], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[3], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[4], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[5], !(structPtr7segCpy->seg7_off_state));
		break;

		case 3:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[0], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[1], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[3], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[5], !(structPtr7segCpy->seg7_off_state));
		break;

		case 4:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[1], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[5], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[6], !(structPtr7segCpy->seg7_off_state));
		break;

		case 5:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[0], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[3], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[5], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[6], !(structPtr7segCpy->seg7_off_state));
		break;

		case 6:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[0], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[3], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[4], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[5], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[6], !(structPtr7segCpy->seg7_off_state));
		break;

		case 7:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[0], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[1], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
		break;

		case 8:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[0], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[1], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[3], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[4], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[5], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[6], !(structPtr7segCpy->seg7_off_state));
		break;

		case 9:
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[0], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[1], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[2], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[5], !(structPtr7segCpy->seg7_off_state));
			DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[6], !(structPtr7segCpy->seg7_off_state));
		break;
	} // switch()

}

void seg7_vidTurnOff(Seg7_cptr_t structPtr7segCpy)
{
	for (u8 i = 0; i < SEG7_SEGMENTS_COUNT_; i++)
		DIO_vidSet_pinValue(structPtr7segCpy->port7Seg, structPtr7segCpy->pin7Seg[i], structPtr7segCpy->seg7_off_state);
}

